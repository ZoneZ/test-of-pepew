from django.test import TestCase, Client
from django.urls import resolve

from .views import book_list


class Story9UnitTests(TestCase):

    def test_book_list_url_exists(self):
        response = Client().get('/book/list')
        self.assertEqual(200, response.status_code)

    def test_book_list_uses_correct_function(self):
        response = resolve('/book/list')
        self.assertEqual(response.func, book_list)

    def test_book_list_uses_correct_template(self):
        response = Client().get('/book/list')
        self.assertTemplateUsed(response, 'book-list.html') 