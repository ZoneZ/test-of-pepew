from django.apps import AppConfig  # pragma: no cover


class BookListConfig(AppConfig):  # pragma: no cover
    name = 'book_list'
