from django.conf.urls import url

from .views import book_list, token_verification, \
    clear_session, add_session_favorite, delete_session_favorite

urlpatterns = [
    url(r'^list$', book_list, name='book-list'),
    url(r'^token-verif$', token_verification, name='token-verif'),
    url(r'^clear-session$', clear_session, name='clear-session'),
    url(r'^add-favorite/(?P<book_id>[-\w]+)', add_session_favorite, name='add-favorite'),
    url(r'^delete-favorite/(?P<book_id>[-\w]+)', delete_session_favorite, name='delete-favorite'),
]