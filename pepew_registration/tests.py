import json
from django.test import TestCase, Client
from django.urls import resolve

from .forms import SubscriberForm
from .models import Subscriber
from .views import register_page


class Story10UnitTests(TestCase):

    def setUp(self):
        Subscriber(
            name='test',
            email='test@gmail.com',
            password='passwordtest'
        ).save()

    def test_registration_page_url_exists(self):
        response = Client().get('/register/')
        self.assertEqual(200, response.status_code)

    def test_registration_page_uses_correct_function(self):
        response = resolve('/register/')
        self.assertEqual(register_page, response.func)

    def test_registration_page_uses_correct_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register_page.html')

    def test_subscriber_model_can_create_new_instance(self):
        Subscriber.objects.create(
            email='abcdefg@mail.com',
            name='ancolmania',
            password='inipasswordbohongan123'
        )
        count_subscribers = Subscriber.objects.all().count()
        self.assertEqual(count_subscribers, 2)

    def test_subscriber_form_input_has_css_classes(self):
        form = SubscriberForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_email"', form.as_p())
        self.assertIn('id="id_name"', form.as_p())
        self.assertIn('id="id_password1"', form.as_p())
        self.assertIn('id="id_password2"', form.as_p())

    def test_form_validation_for_exceeding_username_length(self):
        form = SubscriberForm(data={
            'name': 'a'*300
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'], ['Panjang maksimal nama adalah 25 karakter.']
        )

    def test_form_validation_for_blank_username(self):
        form = SubscriberForm(data={
            'name': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'], ['Nama tidak boleh kosong.']
        )

    def test_form_validation_for_already_registered_email(self):
        response = Client().get('/register/validate-email/', {'email': 'test@gmail.com'})
        json_data = json.loads(response.content.decode('utf8'))
        self.assertTrue(json_data['is_taken'])
        self.assertEqual(json_data['message'], 'Email tersebut sudah pernah didaftarkan sebelumnya, '
                                               'silahkan daftar dengan email lain.')

    def test_form_validation_for_valid_email(self):
        response = Client().get('/register/validate-email/', {'email': 'test2@gmail.com'})
        json_data = json.loads(response.content.decode('utf8'))
        self.assertFalse(json_data['is_taken'])
        self.assertEqual(json_data['message'], 'Email belum terdaftar, silahkan daftar dengan email ini.')

    def test_form_successful_submission_response(self):
        response = Client().post('/register/add-subscriber/', {
            'name': 'Whoooo?',
            'email': 'test2@gmail.com',
            'password1': 'snaaaaaaaaake!',
            'password2': 'snaaaaaaaaake!',
        })
        json_data = json.loads(response.content.decode('utf8'))
        self.assertTrue(json_data['success'])
        self.assertEqual(json_data['log'], 'Data berhasil disimpan')

    def test_form_unsuccessful_submission_response(self):
        response = Client().post('/register/add-subscriber/', {
            'name': 'Whoooo?',
            'email': 'test2@gmail.com',
            'password1': 'snaaaaaaaaake!',
            'password2': 'liquidddddddd!',
        })
        json_data = json.loads(response.content.decode('utf8'))
        self.assertFalse(json_data['success'])

    def test_form_validation_for_exceeding_password_length(self):
        form = SubscriberForm(data={
            'password1': 'a'*300,
            'password2': 'a'*300,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password2'], ['Panjang maksimal password adalah 128 karakter.']
        )

    def test_form_validation_for_password_shorter_than_8_chars(self):
        form = SubscriberForm(data={
            'password1': 'a',
            'password2': 'a',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password2'], ['Password kurang dari 8 karakter.']
        )

    def test_form_validation_for_full_numeric_password(self):
        form = SubscriberForm(data={
            'password1': 123456789,
            'password2': 123456789,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password2'], ['Password tidak boleh sepenuhnya berupa angka.']
        )

    def test_form_validation_for_different_passwords(self):
        form = SubscriberForm(data={
            'name': 'ancolmania',
            'password1': 'password1',
            'password2': 'password2',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password2'], ['Kedua password tidak sama.']
        )

    def test_subscriber_instance_is_created_after_successful_form_input(self):
        form = SubscriberForm(data={
            'name': 'ancolmania',
            'password1': 'passwordku',
            'password2': 'passwordku',
            'email': 'ancol@mail.com',
        })
        self.assertTrue(form.is_valid())
        form.save()
        count_subscriber = Subscriber.objects.count()
        self.assertEqual(count_subscriber, 2)

    def test_page_redirect_to_form_for_http_get_on_add_subscriber_view(self):
        response = Client().get('/register/add-subscriber/')
        self.assertRedirects(response, '/register/') 