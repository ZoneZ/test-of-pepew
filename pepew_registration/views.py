import json

from django.http import HttpResponse
from django.shortcuts import render, redirect

from .models import Subscriber
from .forms import SubscriberForm

response = {}


def register_page(request):
    response['username'] = []
    if 'username' in request.session.keys():
        response['username'] = request.session['username']
    response['form'] = SubscriberForm()
    return render(request, 'register_page.html', response)


def validate_email(request):
    email = request.GET.get('email', None)
    email_is_taken = Subscriber.objects.filter(email__iexact=email).exists()
    data = {'is_taken': email_is_taken}
    if data['is_taken']:
        data['message'] = 'Email tersebut sudah pernah didaftarkan sebelumnya, silahkan daftar dengan email lain.'
    else:
        data['message'] = 'Email belum terdaftar, silahkan daftar dengan email ini.'
    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type='application/json')


def add_subscriber(request):
    data = {}
    if request.method == 'POST':
        subscriber_form = SubscriberForm(request.POST)
        if subscriber_form.is_valid():
            subscriber_form.save()
            data['log'] = 'Data berhasil disimpan'
            data['success'] = True
            json_data = json.dumps(data)
        else:
            errors = dict([(key, [error for error in value]) for key, value in subscriber_form.errors.items()])
            data['log'] = errors
            data['success'] = False
            json_data = json.dumps(data)
        return HttpResponse(json_data, content_type='application/json')
    else:
        return redirect('registration:register')


def get_subscribers(request):
    subscribers = Subscriber.objects.all()
    subscribers_list = [{'id': obj.id, 'name': obj.name, 'email': obj.email}
                        for obj in subscribers]
    return HttpResponse(json.dumps({'subscribers': subscribers_list}),
                        content_type='application/json')


def unsubscribe(request):
    if request.method == 'POST':
        subscriber_id = request.POST['subscriber_id']
        Subscriber.objects.get(id=subscriber_id).delete()
        return HttpResponse(json.dumps({'message': 'Berhasil unsubscribe!'}), content_type='application/json')
    else:
        return redirect('registration:register')