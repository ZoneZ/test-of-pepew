from django.apps import AppConfig  # pragma: no cover


class PepewRegistrationConfig(AppConfig):  # pragma: no cover
    name = 'pepew_registration'
