from django.conf.urls import url

from .views import register_page, validate_email, add_subscriber, get_subscribers, unsubscribe

urlpatterns = [
    url(r'^$', register_page, name='register'),
    url(r'^validate-email/$', validate_email, name='validate-email'),
    url(r'^add-subscriber/$', add_subscriber, name='add-subscriber'),
    url(r'^get-subscribers/$', get_subscribers, name='get-subscribers'),
    url(r'^unsubscribe/$', unsubscribe, name='unsubscribe'),
]