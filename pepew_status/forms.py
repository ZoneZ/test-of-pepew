from django import forms

from .models import Status


class StatusForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(StatusForm, self).__init__(*args, **kwargs)

        self.fields['status'].label = 'Status'
        self.fields['status'].widget = forms.Textarea(attrs={'class': 'form-control',
                                                             'placeholder': 'Masukkan status Anda.',
                                                             'rows': 5,
                                                             'cols': 8}
                                                      )
        self.fields['status'].error_messages = {'max_length': 'Status tidak boleh melebihi 300 karakter.',
                                                'required': 'Status harus diisi.'}

    class Meta:
        model = Status
        fields = ['status']