from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .forms import StatusForm
from .models import Status

response = {}


def index(request):
    response['status'] = Status.objects.all()
    response['form'] = StatusForm()
    response['username'] = []
    if 'username' in request.session.keys():
        response['username'] = request.session['username']
    html = 'index.html'
    return render(request, html, response)


def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('status:index'))
    else:
        form = StatusForm()
    return render(request, 'index.html', {'form': form,
                                          'status': Status.objects.all()})


def profile(request):
    response['username'] = []
    if 'username' in request.session.keys():
        response['username'] = request.session['username']
    return render(request, 'profile.html', response)

