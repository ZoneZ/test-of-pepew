import datetime
import time

from django.conf import settings
from importlib import import_module
from unittest import mock

import pytz
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select

from .forms import StatusForm
from .models import Status
from .views import add_status, index, profile


class PepewStatusUnitTest(TestCase):

    def test_landing_page_exists(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url_not_found(self):
        response = Client().get('/status/ancol/')
        self.assertEqual(response.status_code, 404)

    def test_root_url_redirects_to_index(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/status/', 302, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_landing_page_uses_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello apa kabar?', html_response)

    def test_model_can_create_new_status(self):
        Status.objects.create(
            status='Ancol',
            created_at=timezone.now()
        )
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)

    def test_model_date_auto_now_add_works(self):
        mocked_time = datetime.datetime(2018, 10, 10, 1, 1, 1, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_time)):
            new_status = Status.objects.create()
            self.assertEqual(new_status.created_at, mocked_time)

    def test_form_status_input_has_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('rows="5"', form.as_p())
        self.assertIn('cols="8"', form.as_p())

    def test_form_validation_for_blank_status(self):
        form = StatusForm(data={
            'status': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ['Status harus diisi.']
        )

    def test_form_validation_for_exceeding_status_length(self):
        form = StatusForm(data={
            'status': '9bmGpyUHaFUibqVKs5pZNz4f8AfcH80cco4HIobnfld1mgdmhSVhm3qDOUwHsC9vxhal6tdvbmrd3gR5K4UJxS1mA9bw2PO1IGLU3HHulltB6aFOIz5vHJjEDiKB9djeBh54hsjOVZGWjTVHNP24vD160hSEuAD7HeKLNICd6rIYeFeR1MVJh6aupB0showgZpJcE8DUDKHbPVEvK809zQr3Dzy4rR9d4rpEzA9x4Qo9ecxec9xpXyd4BbOuwUOVW32TOsKATig4qk5YJny6jDxX9NWGvomqsNb9mZfoJlFG6',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ['Status tidak boleh melebihi 300 karakter.']
        )

    def test_form_validation_for_maximum_status_length(self):
        form = StatusForm(data={
            'status': '9bmGpyUHaFUibqVKs5pZNz4f8AfcH80cco4HIobnfld1mgdmhSVhm3qDOUwHsC9vxhal6tdvbmrd3gR5K4UJxS1mA9bw2PO1IGLU3HHulltB6aFOIz5vHJjEDiKB9djeBh54hsjOVZGWjTVHNP24vD160hSEuAD7HeKLNICd6rIYeFeR1MVJh6aupB0showgZpJcE8DUDKHbPVEvK809zQr3Dzy4rR9d4rpEzA9x4Qo9ecxec9xpXyd4BbOuwUOVW32TOsKATig4qk5YJny6jDxX9NWGvomqsNb9mZfoJlFG',
        })
        self.assertTrue(form.is_valid())

    def test_post_status_using_add_status_func(self):
        found = resolve('/status/add-status/')
        self.assertEqual(found.func, add_status)

    def test_get_form_function(self):
        response = Client().get('/status/add-status/')
        self.assertEqual(response.status_code, 200)

    def test_post_success_and_render_the_result(self):
        test_status = 'Ancol'
        response_post = Client().post('/status/add-status/',
                                      {
                                          'status': test_status
                                      })
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test_status, html_response)

    def test_post_error_and_render_the_result(self):
        test_status = 'Ancol'
        response_post = Client().post('/status/add-status/',
                                      {
                                          'status': ''
                                      })
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test_status, html_response)

    def test_post_success_and_exists_in_the_database(self):
        Client().post('/status/add-status/',
                                      {
                                          'status': 'Test'
                                      })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/status/add-status/',
                                      {
                                          'status': ''
                                      })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 0)

    def test_profile_page_exist(self):
        response = Client().get('/status/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_profile_func(self):
        found = resolve('/status/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_url_redirection(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/status/profile/', 302, 200)

    def test_profile_page_uses_template(self):
        response = Client().get('/status/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_page_has_title_element(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>', html_response)

    def test_profile_page_has_navbar(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('nav class="navbar navbar-expand-lg main-navbar"', html_response)
        self.assertIn('class="navbar-brand navbar-button"', html_response)

    def test_profile_page_contains_self_image_element(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<img class="mx-auto d-block avatar-image"', html_response)

    def test_profile_page_contains_about_texts(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Know Me Deeper', html_response)
        self.assertIn('Hello, I am Farhan Azyumardhi Azmi.', html_response)
        self.assertIn('I’m currently studying Computer Science Major at University of Indonesia.', html_response)
        self.assertIn('I like to learn new opportunities.', html_response)

    def test_profile_page_contains_academic_div_element(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="container mx-auto d-block academic-div">', html_response)

    def test_profile_page_contains_project_div_element(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="container mx-auto d-block project-div">', html_response)

    def test_profile_page_contains_gskill_div_element(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="container mx-auto d-block skill-div">', html_response)

    def test_profile_page_contains_skill_logo_div_class(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('class="container skill-logo-div"', html_response)


# class Story7FunctionalTest(StaticLiveServerTestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story7FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story7FunctionalTest, self).tearDown()
#
#     def test_input_status_coba_coba(self):
#         selenium = self.selenium
#         selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com')
#
#         time.sleep(3)  # Makes the user actually see the page before proceeding
#
#         input_string = 'Coba Coba'
#         status_field = selenium.find_element_by_id('id_status')
#         submit_button = selenium.find_element_by_id('status-submit-button')
#         status_field.send_keys(input_string)
#
#         time.sleep(3)  # Makes the user actually see the page before proceeding
#
#         submit_button.click()
#
#         time.sleep(3)  # Makes the user actually see the page before proceeding
#
#         status_col = selenium.find_elements_by_id('status-col-status')[-1]
#         self.assertEqual(input_string, status_col.text)
#
#     def test_navbar_button_has_correct_css_sizes_and_layouting(self):
#         selenium = self.selenium
#         selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com')
#
#         navbar_button = selenium.find_element_by_class_name('navbar-button')
#
#         navbar_button_width = navbar_button.value_of_css_property('width')
#         navbar_button_box_sizing = navbar_button.value_of_css_property('box-sizing')
#         navbar_button_border_radius = navbar_button.value_of_css_property('border-radius')
#
#         self.assertEqual('100px', navbar_button_width)
#         self.assertEqual('border-box', navbar_button_box_sizing)
#         self.assertEqual('20px', navbar_button_border_radius)
#
#     def test_profile_page_main_text_has_correct_css_stylings(self):
#         selenium = self.selenium
#         selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com/profile/')
#
#         main_text = selenium.find_element_by_class_name('text-main')
#
#         main_text_font_family = main_text.value_of_css_property('font-family')
#         main_text_font_style = main_text.value_of_css_property('font-style')
#         main_text_font_weight = main_text.value_of_css_property('font-weight')
#         main_text_line_height = main_text.value_of_css_property('line-height')
#         main_text_text_align = main_text.value_of_css_property('text-align')
#         main_text_pading = main_text.value_of_css_property('padding')
#
#         self.assertEqual('Bitter, serif', main_text_font_family)
#         self.assertEqual('normal', main_text_font_style)
#         self.assertEqual('700', main_text_font_weight)
#         self.assertEqual('normal', main_text_line_height)
#         self.assertEqual('center', main_text_text_align)
#         self.assertEqual('20px', main_text_pading)
#
#     def test_profile_page_avatar_image_has_correct_positioning(self):
#         selenium = self.selenium
#         selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com/profile/')
#
#         avatar_image = selenium.find_element_by_class_name('avatar-image')
#         avatar_image_location = avatar_image.location
#         avatar_image_size = avatar_image.size
#
#         self.assertEqual(78, avatar_image_location['y'])
#         self.assertEqual(410, avatar_image_location['x'])
#         self.assertEqual(200, avatar_image_size['height'])
#         self.assertEqual(200, avatar_image_size['width'])
#
#     def test_profile_page_text_main_has_correct_positioning(self):
#         selenium = self.selenium
#         selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com/profile/')
#
#         text_main = selenium.find_element_by_class_name('text-main')
#         text_main_location = text_main.location
#         text_main_size = text_main.size
#
#         self.assertEqual(298, text_main_location['y'])
#         self.assertEqual(0, text_main_location['x'])
#         self.assertEqual(1019, text_main_size['width'])
#         self.assertEqual(88, text_main_size['height'])


# class Story8FunctionalTest(StaticLiveServerTestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story8FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story8FunctionalTest, self).tearDown()
#
#     # def test_theme_selector_works_successfully(self):
#     #     selenium = self.selenium
#     #     selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com')
#     #
#     #     page_background = selenium.find_element_by_tag_name('body')
#     #     theme_selector = Select(selenium.find_element_by_class_name('select-theme'))
#     #     theme_change_button = selenium.find_element_by_class_name('apply-theme')
#     #
#     #     theme_selector.select_by_visible_text('Default')
#     #     theme_change_button.send_keys(Keys.RETURN)
#     #     time.sleep(2)
#     #     page_background_color = page_background.value_of_css_property('background')
#     #     self.assertIn('rgba(255, 107, 107, 0.68)', page_background_color)
#     #
#     #     theme_selector.select_by_visible_text('Indigo')
#     #     theme_change_button.send_keys(Keys.RETURN)
#     #     time.sleep(2)
#     #     page_background_color = page_background.value_of_css_property('background')
#     #     self.assertIn('rgb(63, 81, 181)', page_background_color)
#     #
#     #     theme_selector.select_by_visible_text('Lime')
#     #     theme_change_button.send_keys(Keys.RETURN)
#     #     time.sleep(2)
#     #     page_background_color = page_background.value_of_css_property('background')
#     #     self.assertIn('rgb(205, 220, 57)', page_background_color)
#
#     # def test_accordion_works_successfully(self):
#     #     selenium = self.selenium
#     #     selenium.get('https://ppw-c-testing-goat-lair.herokuapp.com/status/profile/')
#     #
#     #     accordions = selenium.find_elements_by_class_name('accordion')
#     #
#     #     activity = accordions[0]
#     #     activity_header = activity.find_element_by_class_name('accordion-header')
#     #     self.assertNotIn('active', activity_header.get_attribute('class'))
#     #     activity.click()
#     #     activity_content = activity.find_element_by_class_name('accordion-content')
#     #     time.sleep(2)
#     #     activity_header = activity.find_element_by_class_name('accordion-header')
#     #     self.assertIn('active', activity_header.get_attribute('class'))
#     #     activity_content_list = activity_content.find_element_by_tag_name('ul').\
#     #         find_elements_by_tag_name('li')
#     #     self.assertEqual('Kuliah', activity_content_list[0].text)
#     #     self.assertEqual('Mengerjakan tugas', activity_content_list[1].text)
#     #     self.assertEqual('Tidur', activity_content_list[2].text)
#     #
#     #     experience = accordions[1]
#     #     experience_header = experience.find_element_by_class_name('accordion-header')
#     #     self.assertNotIn('active', experience_header.get_attribute('class'))
#     #     experience.click()
#     #     experience_content = experience.find_element_by_class_name('accordion-content')
#     #     time.sleep(2)
#     #     experience_header = experience.find_element_by_class_name('accordion-header')
#     #     self.assertIn('active', experience_header.get_attribute('class'))
#     #     self.assertIn('Organisasi', experience_content.find_elements_by_tag_name('ul')[0].text)
#     #     experience_content_first_list = experience_content.find_elements_by_tag_name('ul')[0].\
#     #         find_element_by_tag_name('li')
#     #     self.assertEqual('Staff Biro IT Force FUKI Fasilkom UI 2018', experience_content_first_list.text)
#     #     self.assertIn('Kepanitiaan', experience_content.find_elements_by_tag_name('ul')[1].text)
#     #     experience_content_second_list = experience_content.find_elements_by_tag_name('ul')[1].\
#     #         find_elements_by_tag_name('li')
#     #     self.assertEqual('Staff Divisi Perlengkapan BETIS 2018', experience_content_second_list[0].text)
#     #     self.assertEqual('Staff Divisi Penunjang SIWAK-NG 2018', experience_content_second_list[1].text)
#     #
#     #     achievement = accordions[2]
#     #     achievement_header = achievement.find_element_by_class_name('accordion-header')
#     #     self.assertNotIn('active', achievement_header.get_attribute('class'))
#     #     achievement.click()
#     #     achievement_content = achievement.find_element_by_class_name('accordion-content')
#     #     time.sleep(2)
#     #     achievement_header = achievement.find_element_by_class_name('accordion-header')
#     #     self.assertIn('active', achievement_header.get_attribute('class'))
#     #     self.assertEqual('Belum Ada :^(', achievement_content.text)