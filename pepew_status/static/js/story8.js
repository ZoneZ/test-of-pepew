var themes = [
    {'id': 0, 'text': 'Default', 'bgColor': 'rgba(255, 107, 107, 0.68)'},
    {'id': 1, 'text': 'Indigo', 'bgColor': '#3F51B5'},
    {'id': 2, 'text': 'Lime', 'bgColor': '#CDDC39'},
];

$(window).bind('load', function () {
    $('#page-loading').fadeOut(1000);
});

$('document').ready(function () {

    $('.accordion').on('click', '.accordion-header', function () {
        $(this).toggleClass('active').next().slideToggle();
    });

    if (localStorage.getItem('selectedTheme')) {
        $('body').css({
            'background': JSON.parse(localStorage.getItem('selectedTheme')).bgColor
        });
    }

    $('.select-theme').select2({
        data: themes,
    }).next().remove();

    $('.apply-theme').click(function () {
       var themeID = $('.select-theme').val();
       $('body').css({
           'background': themes[themeID].bgColor
       });
       localStorage.setItem('selectedTheme', JSON.stringify(themes[themeID]));
    });
});