from django.apps import AppConfig  # pragma: no cover


class PepewStatusConfig(AppConfig):  # pragma: no cover
    name = 'pepew_status'
